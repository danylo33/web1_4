<?php
class Student {
    public $group;
    public $firstName;
    public $lastName;
    public $birthDate;
    public $gender;

    public function __construct($group, $firstName, $lastName, $birthDate, $gender) {
        $this->group = $group;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->birthDate = $birthDate;
        $this->gender = $gender;
    }
}


?>