document.addEventListener('DOMContentLoaded', function () {
  
  const openModalBtn = document.getElementById('open-modal');
  const closeModalBtn = document.getElementById('close-modal');
  const okBtn = document.getElementById('ok-button')
  const addModal = document.getElementById('add-modal');
  const editButton = document.getElementById("edit-btn");

  var deleteModal = document.getElementById("delete-user-modal");

  var span = document.getElementsByClassName("modal-close")[0];
  var cancelBtn = document.getElementsByClassName("modal-cancel")[0];
  var okButton = document.getElementsByClassName("modal-ok")[0];

  const studentsTable = document.querySelector("#studentsTable tbody");
  let studentsJSON = []
  let mode = '';
  let LN = '';
  let row;

  function getStudents() {
    

      fetch('http://localhost:80/Lab-3/get.php', {
              method: 'GET',
              headers: {
                'Content-Type': 'application/json'
              }
            }).then(response => response.json())
              .then(result => {
                console.log(result);
                result.forEach(student => {
                  const newRow = document.createElement('tr');
                  const newCheckboxCell = document.createElement('td');
                  const newCheckbox = document.createElement('input');
                  newCheckbox.setAttribute('type', 'checkbox');
                  newCheckboxCell.appendChild(newCheckbox);
                  newRow.appendChild(newCheckboxCell);
                  for (let i = 1; i <= 6; i++) {
                    const newTextCell = document.createElement('td');
                    let newElement;
                    if (i === 1) {
                      newElement = document.createTextNode(student.studentGroup);
                    }
                    else if (i === 2) {
                      newElement = document.createTextNode(student.lastName + " " + student.firstName);
                    }
                    else if (i === 3) {
                      newElement = document.createTextNode(student.gender);
                    }
                    else if (i === 4) {
                      newElement = document.createTextNode(student.birthDate);
                    }
                    else if (i === 5) {
                      newElement = document.createElement("button");
                      newElement.style.height = "20px"
                      newElement.style.width = "20px"
                      newElement.style.borderRadius = "50%"
                      newElement.addEventListener('mouseover', function () {
                        newElement.style.backgroundColor = "lightgreen";
                      })
                      newElement.addEventListener('mouseout', function () {
                        newElement.style.backgroundColor = "white";
                      })
                    }
                    else if (i === 6) {
        
                      newElement = document.createElement("button");
                      let secondElement = document.createElement("button");
        
                      let firstImage = document.createElement("img");
                      firstImage.src = "https://cdn-icons-png.flaticon.com/512/2976/2976286.png";
                      firstImage.style.height = "18px";
                      firstImage.style.width = "18px";
        
        
                      newElement.style.height = "35px";
                      newElement.style.width = "35px";
                      newElement.style.marginLeft = "3px"
        
                      let secondImage = document.createElement("img");
                      secondImage.src = "https://cdn-icons-png.flaticon.com/512/1250/1250615.png";
                      secondImage.style.height = "18px";
                      secondImage.style.width = "18px";
        
        
                      secondElement.style.height = "35px";
                      secondElement.style.width = "35px";
                      secondElement.style.marginRight = "3px"
                      secondElement.style.alignItems = "center"
                      secondElement.style.justifyContent = "center"
        
                      secondElement.appendChild(secondImage);
                      newElement.appendChild(firstImage);
        
                      newElement.setAttribute('class', 'delete-buttons');
                      secondElement.setAttribute('class', 'edit-buttons')
        
                      newTextCell.appendChild(secondElement);
                    }
                    newTextCell.appendChild(newElement);
                    newRow.appendChild(newTextCell);
                    // addModal.style.display = 'none';
        
                  }
                  studentsTable.appendChild(newRow);
                });
              })

        
  }

  getStudents();
  

  openModalBtn.addEventListener('click', function () {
    addModal.style.display = 'block';
    document.getElementById("modal-header").innerHTML = "Add student";
    mode = "ADD";
  });

  closeModalBtn.addEventListener('click', function () {
    addModal.style.display = 'none';
  });


  okBtn.addEventListener('click', function () {
    if (mode === "ADD") {
      const newRow = document.createElement('tr');
      const newCheckboxCell = document.createElement('td');
      const newCheckbox = document.createElement('input');
      newCheckbox.setAttribute('type', 'checkbox');
      newCheckboxCell.appendChild(newCheckbox);
      newRow.appendChild(newCheckboxCell);

      const group = document.getElementById("group").value;
      const firstName = document.getElementById("first-name").value;
      const lastName = document.getElementById("last-name").value;
      const gender = document.getElementById("gender").value;
      const birthDate = document.getElementById("birth-date").value;

      // var re = /^[A-Za-z]+$/;

      // // Validate the values of the input fields
      // if (group === '') {
      //   alert('Please select a group.');
      //   return false;
      // }

      // if (firstName === '') {
      //   alert('Please enter your first name.');
      //   return false;
      // }
      // else if (re.test(firstName) === false){
      //   alert('Name should only contain letters.')
      //   return false;
      // }

      // if (lastName === '') {
      //   alert('Please enter your last name.');
      //   return false;
      // }
      // else if (re.test(lastName) === false){
      //   alert('Name should only contain letters.')
      //   return false;
      // }

      // if (gender === '') {
      //   alert('Please select a gender.');
      //   return false;
      // }

      // if (birthDate === '') {
      //   alert('Please enter your birth date.');
      //   return false;
      // }

      studentObj = {
        group: group,
        first_name: firstName,
        last_name: lastName,
        gender: gender,
        birth_date: birthDate
      }
      studentJSON = JSON.stringify(studentObj)

      // $.ajax({
      //   type: 'POST',
      //   url: 'index.php',
      //   data: {
      //     studentObj
      //   },
      //   success: function (response) {
      //     // Handle success response here
      //   },
      //   error: function (jqXHR, textStatus, errorThrown) {
      //     // Handle error here
      //   }
      // });

      studentObj = {
        group: group,
        firstName: firstName,
        lastName: lastName,
        birthDate: birthDate,
        gender: gender
      }
      let errors = false;
      fetch('http://localhost:80/Lab-3/index.php', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(studentObj)
      })
        .then(response => response.json())
        .then(result => {
          if (result.errors && result.errors.length > 0) {
            // Display error messages using alert
            errors = true;
            console.log(errors)
            alert('Errors found:\n' + result.errors.join('\n'));
          } else if (result.message) {
            // Display the success message
            alert(result.message);
          }

          if (!errors) {
            fetch('http://localhost:80/Lab-3/controller.php', {
              method: 'POST',
              headers: {
                'Content-Type': 'application/json'
              },
              body: JSON.stringify(studentObj)
            }).then(response => response.text())
              .then(result => {
                console.log(result);
              })
            for (let i = 1; i <= 6; i++) {
              const newTextCell = document.createElement('td');
              let newElement;
              if (i === 1) {
                newElement = document.createTextNode(group);
              }
              else if (i === 2) {
                newElement = document.createTextNode(lastName + " " + firstName);
              }
              else if (i === 3) {
                newElement = document.createTextNode(gender);
              }
              else if (i === 4) {
                newElement = document.createTextNode(birthDate);
              }
              else if (i === 5) {
                newElement = document.createElement("button");
                newElement.style.height = "20px"
                newElement.style.width = "20px"
                newElement.style.borderRadius = "50%"
                newElement.addEventListener('mouseover', function () {
                  newElement.style.backgroundColor = "lightgreen";
                })
                newElement.addEventListener('mouseout', function () {
                  newElement.style.backgroundColor = "white";
                })
              }
              else if (i === 6) {

                newElement = document.createElement("button");
                let secondElement = document.createElement("button");

                let firstImage = document.createElement("img");
                firstImage.src = "https://cdn-icons-png.flaticon.com/512/2976/2976286.png";
                firstImage.style.height = "18px";
                firstImage.style.width = "18px";


                newElement.style.height = "35px";
                newElement.style.width = "35px";
                newElement.style.marginLeft = "3px"

                let secondImage = document.createElement("img");
                secondImage.src = "https://cdn-icons-png.flaticon.com/512/1250/1250615.png";
                secondImage.style.height = "18px";
                secondImage.style.width = "18px";


                secondElement.style.height = "35px";
                secondElement.style.width = "35px";
                secondElement.style.marginRight = "3px"
                secondElement.style.alignItems = "center"
                secondElement.style.justifyContent = "center"

                secondElement.appendChild(secondImage);
                newElement.appendChild(firstImage);

                newElement.setAttribute('class', 'delete-buttons');
                secondElement.setAttribute('class', 'edit-buttons')

                newTextCell.appendChild(secondElement);
              }
              newTextCell.appendChild(newElement);
              newRow.appendChild(newTextCell);
              addModal.style.display = 'none';

            }
            studentsTable.appendChild(newRow);
      

            document.getElementById("group").value = "PZ-21";
            document.getElementById("last-name").value = "";
            document.getElementById("first-name").value = "";
            document.getElementById("gender").value = "M";
            document.getElementById("birth-date").value = "2000-01-01";
          }
        })
        .catch(error => {
          // Handle any errors that occurred during the request
          console.error(error);
        })

      // console.log(errors);


      studentJSON = JSON.stringify(studentObj)
      studentsJSON.push(studentJSON)





    }
    else if (mode === "EDIT") {
      const group = document.getElementById("group").value;
      const firstName = document.getElementById("first-name").value;
      const lastName = document.getElementById("last-name").value;
      const gender = document.getElementById("gender").value;
      const birthDate = document.getElementById("birth-date").value;

      let errors = false;

      // var re = /^[A-Za-z]+$/;

      // // Validate the values of the input fields
      // if (group === '') {
      //   alert('Please select a group.');
      //   return false;
      // }

      // if (firstName === '') {
      //   alert('Please enter your first name.');
      //   return false;
      // }
      // else if (re.test(firstName) === false) {
      //   alert('Name should only contain letters.')
      //   return false;
      // }

      // if (lastName === '') {
      //   alert('Please enter your last name.');
      //   return false;
      // }
      // else if (re.test(lastName) === false) {
      //   alert('Name should only contain letters.')
      //   return false;
      // }

      // if (gender === '') {
      //   alert('Please select a gender.');
      //   return false;
      // }

      // if (birthDate === '') {
      //   alert('Please enter your birth date.');
      //   return false;
      // }
      studentObj = {
        group: group,
        firstName: firstName,
        lastName: lastName,
        birthDate: birthDate
      }
      fetch('http://localhost:80/Lab-3/index.php', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(studentObj)
      })
        .then(response => response.json())
        .then(result => {
          if (result.errors && result.errors.length > 0) {
            // Display error messages using alert
            errors = true;
            console.log(errors)
            alert('Errors found:\n' + result.errors.join('\n'));
          } else if (result.message) {
            // Display the success message
            alert(result.message);
          }

          const editObj = {
            group: group,
        firstName: firstName,
        lastName: lastName,
        birthDate: birthDate,
        gender: gender,
        updateLastName: LN

          }
console.log(editObj);
          fetch('http://localhost:80/Lab-3/update.php', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(editObj)
      }).then(response => response.text()).
      then((result) => console.log(result))
      .catch((error) => console.log(error));

          if (!errors) {
            row.cells[1].innerHTML = group;
            row.cells[2].innerHTML = lastName + " " + firstName;
            row.cells[3].innerHTML = gender;
            row.cells[4].innerHTML = birthDate;
          }
          else {
            return;
          }
        })
        .catch(error => {
          // Handle any errors that occurred during the request
          console.error(error);
        })





      document.getElementById("group").value = "PZ-21";
      document.getElementById("last-name").value = "";
      document.getElementById("first-name").value = "";
      document.getElementById("gender").value = "M";
      document.getElementById("birth-date").value = "2000-01-01";
      //addModal.style.display = 'none';
    }




    var deleteButtons = document.getElementsByClassName("delete-buttons")
    var editButtons = document.getElementsByClassName("edit-buttons")

    for (var i = 0; i < deleteButtons.length; i++) {
      deleteButtons[i].addEventListener("click", function () {
        deleteModal.style.display = "block";
        var row = this.closest("tr");
        var cells = row.cells;
        var thirdCell = cells[2];
        const thirdCellText = thirdCell.textContent;
        const name = thirdCellText.split(" ");
        
        okButton.onclick = function () {
          const obj = {
          lastName : name[0]
          };
          fetch('http://localhost:80/Lab-3/delete.php', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(obj)
      })
        .then(response => response.json())
          row.parentNode.removeChild(row);
          deleteModal.style.display = "none";
        }

      });
    }

    for (var i = 0; i < editButtons.length; i++) {
      editButtons[i].addEventListener("click", function () {
        addModal.style.display = 'block';
        document.getElementById("modal-header").innerHTML = "Edit student";
        row = this.closest("tr");
        var cells = row.cells;
        var thirdCell = cells[2];
        const thirdCellText = thirdCell.textContent;
        const name = thirdCellText.split(" ");
        LN = name[0];
        mode = "EDIT";
      })
    }


    span.onclick = function () {
      deleteModal.style.display = "none";
    }

    // When the user clicks on Cancel button, close the modal
    cancelBtn.onclick = function () {
      deleteModal.style.display = "none";
    }

    // When the user clicks on OK button, delete the user and close the modal


    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function (event) {
      if (event.target == deleteModal) {
        deleteModal.style.display = "none";
      }
    }

  });



});
