<?php
function getAllStudents()
{
    try {
        $dsn = 'mysql:host=localhost;dbname=students';
        $username = 'root';
        $password = '';

        // Create a new PDO instance
        $pdo = new PDO($dsn, $username, $password);

        // Set error mode to exception
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        // Prepare the SQL statement to select all students
        $stmt = $pdo->prepare("SELECT * FROM students_data");

        // Execute the query
        $stmt->execute();

        // Fetch all the rows as an associative array
        $students = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $students;
    } catch (PDOException $e) {
        echo "Error: " . $e->getMessage();
        return [];
    }
}

// Usage
$students = getAllStudents();

// Send students as JSON to the client
header('Content-Type: application/json');
echo json_encode($students);
?>
