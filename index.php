<?php
  $data = json_decode(file_get_contents('php://input'), true);
  if ($data !== null && isset($data['group']) && isset($data['firstName']) && isset($data['lastName']) && isset($data['birthDate'])) {
    $group = $data['group'];
    $firstName=$data['firstName'];
    $lastName=$data['lastName'];
    $birthDate=$data['birthDate'];
    $pattern = '/^[a-zA-Z\s]+$/';

    if (empty($group)) {
        $errors[] = "Group is required";
      }
    
      if (empty($firstName)) {
        $errors[] = "First Name is required";
      }
      if (empty($lastName)) {
        $errors[] = "Last Name is required";
      }
    
      if (empty($birthDate)) {
        $errors[] = "Birthdate is required";
      }
      if (!preg_match($pattern, $firstName)) {
        $errors[] = "First Name should consist of Latin characters";
      }
      if (!preg_match($pattern, $lastName)) {
        $errors[] = "Last Name should consist of Latin characters";
      }
     
      if (!empty($errors)) {
        // Send error messages as JSON response
        echo json_encode(['errors' => $errors]);
        exit;
      }
      else {
        $response = ['message' => 'Data received successfully'];
        echo json_encode($response);
        exit;
      }
      
  } 
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Golos+Text&display=swap" rel="stylesheet">
    <script src="index.js"></script>
    <title>Document</title>
</head>
<body>
    <header>
        <div class="left">
          <h1>CMS</h1>
        </div>
        <div class="right">
        <div class="tooltip bell-icon">
            <img src="https://cdn-icons-png.flaticon.com/512/3602/3602145.png">
            <div class="tooltiptext">Notification</div>
        </div>
        <div class="tooltip"> 
            <img src="https://cdn-icons-png.flaticon.com/512/727/727399.png">
            <div class="tooltiptext"><span style="margin: 10px;">Profile</span><br><div>Logout</div></div>
        </div>
          
          <h2>John Doe</h2>
        </div>
      </header>
      <div class="main-section">
        <aside>
            <div class="sidebar">
                <ul >
                    <li>Dashboard</li>
                    <li>Students</li>
                    <li>Tasks</li>
                </ul>
            </div>
            
        </aside>
        <div>
            <span>
                <h1>Students</h1>
                <button id="open-modal">+</button>
            </span>

            <div id="add-modal" class="modal">
                <div class="modal-content">
                  <div class="modal-header">
                    <h2 id="modal-header"></h2>
                    <button id="close-modal">&times;</button>
                  </div>
                  <div class="modal-body">
                    <div class="single-input">
                        <div>Group</div>
                        <select name="group" id="group" class="form-input">
                            <option value="PZ-21">PZ-21</option>
                            <option value="PZ-22">PZ-22</option>
                            <option value="PZ-23">PZ-23</option>
                        </select>
                    </div>
                    <div class="single-input">
                        <div >First Name</div>
                        <input type="text" placeholder="" class="form-input" id="first-name"/>
                    </div>
                    <div class="single-input">
                        <div>Last Name</div>
                        <input type="text" placeholder="" class="form-input" id="last-name"/>
                    </div>
                    <div class="single-input">
                        <div>Gender</div>
                        <select name="gender" id="gender" class="form-input">
                            <option value="M">Male</option>
                            <option value="F">Female</option>
                        </select>
                    </div>
                    <div class="single-input"> 
                        <div>Birth Date</div>
                        <input type="date" value="2000-01-01" min="1990-01-01" max="2010-01-01" class="form-input" id="birth-date"/>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button id="ok-button">Ok</button>
                  </div>
                </div>
              </div>

            <!-- Button to trigger the modal -->
    

    <!-- The modal -->
    <div id="delete-user-modal" class="modal">
        <div class="modal-content">
            <div class="modal-header">
                <h2>Warning</h2>
                <span class="modal-close">&times;</span>
                
            </div>
            <div class="modal-body">
                <p style="margin-top:10px; margin-bottom: 10px;">Do you want to delete this user?</p>
            </div>
                <div class="modal-footer">
                    <button class="modal-cancel">Cancel</button>
                    <button class="modal-ok">OK</button>
                </div>
            </div>
    </div>

            
        <table id="studentsTable">
            <tr>
                <th><input type="checkbox"></th>
                <th>Group</th>
                <th>Name</th>
                <th>Gender</th>
                <th>Birthday</th>
                <th>Status</th>
                <th>Options</th>
            </tr>
        </table>
        </div>
        
    </div>
</body>
</html>
