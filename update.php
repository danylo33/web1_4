<?php
require_once 'Student.php'; // Assuming the Person model is defined in "Person.php"

try {
    $data = json_decode(file_get_contents('php://input'), true);
  if ($data !== null && isset($data['group']) && isset($data['firstName']) && isset($data['lastName']) && isset($data['birthDate']) && isset($data['gender'])) {
    $group = $data['group'];
    $firstName=$data['firstName'];
    $lastName=$data['lastName'];
    $birthDate=$data['birthDate'];
    $gender=$data['gender'];
    $updateLastName = $data["updateLastName"];
    $dsn = 'mysql:host=localhost;dbname=students';
        $username = 'root';
        $password = '';

        // Create a new PDO instance
        $pdo = new PDO($dsn, $username, $password);

        // Set error mode to exception
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        // Prepare the SQL statement for update
        $stmt = $pdo->prepare("UPDATE students_data 
                               SET studentGroup = :group, firstName = :firstName, lastName = :lastName, birthDate = :birthDate, gender = :gender 
                               WHERE lastName = :updateLastName");

        // Bind the parameters for update
        $stmt->bindParam(':group', $group);
        $stmt->bindParam(':firstName', $firstName);
        $stmt->bindParam(':lastName', $lastName);
        $stmt->bindParam(':birthDate', $birthDate);
        $stmt->bindParam(':gender', $gender);


        // Execute the update query
        $stmt->execute();

        echo "Data updated successfully.";
  }
}
catch (PDOException $e) {
        echo "Error: " . $e->getMessage();
    }
      
    // Replace with your database credentials
?>
