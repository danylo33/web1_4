<?php
require_once 'Student.php'; // Assuming the Person model is defined in "Person.php"

try {
    $data = json_decode(file_get_contents('php://input'), true);
  if ($data !== null && isset($data['group']) && isset($data['firstName']) && isset($data['lastName']) && isset($data['birthDate']) && isset($data['gender'])) {
    $group = $data['group'];
    $firstName=$data['firstName'];
    $lastName=$data['lastName'];
    $birthDate=$data['birthDate'];
    $gender=$data['gender'];
    $dsn = 'mysql:host=localhost;dbname=students';
    $username = 'root';
    $password = '';

    // Create a new PDO instance
    $pdo = new PDO($dsn, $username, $password);

    // Set error mode to exception
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // Create a new Person instance
    $student = new Student( $group, $firstName, $lastName, $birthDate, $gender);

    // Prepare the SQL statement
    $stmt = $pdo->prepare("INSERT INTO students_data (studentGroup, firstName, lastName, birthDate, gender) 
                           VALUES (:group, :firstName, :lastName, :birthDate, :gender)");

    // Bind the parameters
    $stmt->bindParam(':group', $student->group);
    $stmt->bindParam(':firstName', $student->firstName);
    $stmt->bindParam(':lastName', $student->lastName);
    $stmt->bindParam(':birthDate', $student->birthDate);
    $stmt->bindParam(':gender', $student->gender);

    // Execute the query
    $stmt->execute();

    echo "Data inserted successfully.";
    } 
}
catch (PDOException $e) {
    echo "Error: " . $e->getMessage();
}
      
    // Replace with your database credentials
    
?>
